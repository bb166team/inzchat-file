package pl.bb166.inzchat.file.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pl.bb166.inzchat.file.domain.dto.FileDTO;
import pl.bb166.inzchat.file.service.FileService;
import pl.bb166.inzchat.file.wrapper.ConnectionFileWrapper;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLConnection;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("file")
public class FileController {
    static Logger logger = LoggerFactory.getLogger(FileController.class);

    private FileService fileService;

    @Autowired
    public void setFileService(FileService fileService) {
        this.fileService = fileService;
    }

    @CrossOrigin
    @PostMapping
    @PreAuthorize("hasRole('ROLE_USER')")
    public FileDTO uploadFile(MultipartFile file, @RequestPart(value = "owners", required = false) List<String> owners) throws Exception {
        return fileService.addFileToRepository(file.getOriginalFilename(), file.getInputStream(), owners);
    }

    @CrossOrigin(exposedHeaders = {"Content-disposition"})
    @PreAuthorize("hasRole('ROLE_USER')")
    @GetMapping("{id}")
    public void getFileForId(HttpServletResponse response, @PathVariable Long id, Principal principal) {

        Optional<ConnectionFileWrapper> wrapper = fileService.getFileForId(id, principal.getName());

        wrapper.ifPresent(connection -> {
            try {
                String mime = URLConnection.guessContentTypeFromName(connection.getFileName());
                if (mime == null) {
                    mime = "application/octet-stream";
                }
                response.setContentType(mime);
                response.setContentLength(connection.getFileSize());
                response.setHeader("Content-disposition", "attachment; filename=" + connection.getFileName());

                FileCopyUtils.copy(connection.getInputStream(), response.getOutputStream());
                connection.close();
            } catch (IOException ex) {
                logger.error(ex.getMessage(), ex);
            }
        });


    }
}
