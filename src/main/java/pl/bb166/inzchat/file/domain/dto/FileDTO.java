package pl.bb166.inzchat.file.domain.dto;

import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FileDTO {
    @Getter @Setter
    private Long id;

    @Getter @Setter
    private String filename;
}
