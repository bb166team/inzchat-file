package pl.bb166.inzchat.file.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

@Configuration
public class BootstrapConfiguration {

    @Autowired
    private DataSource dataSource;

    @Bean
    public Connection connection() throws SQLException {
        return dataSource.getConnection();
    }
}
