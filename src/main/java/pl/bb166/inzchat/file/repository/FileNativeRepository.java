package pl.bb166.inzchat.file.repository;

import pl.bb166.inzchat.file.domain.dto.FileDTO;
import pl.bb166.inzchat.file.wrapper.ConnectionFileWrapper;

import java.io.InputStream;
import java.util.List;
import java.util.Optional;

public interface FileNativeRepository {
    Optional<ConnectionFileWrapper> getFile(Long id, String ownerName);
    FileDTO createFile(String filename, InputStream inputStream, List<String> owners);
}