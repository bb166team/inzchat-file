package pl.bb166.inzchat.file.repository;

import org.postgresql.jdbc.PgConnection;
import org.postgresql.largeobject.LargeObject;
import org.postgresql.largeobject.LargeObjectManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pl.bb166.inzchat.file.domain.dto.FileDTO;
import pl.bb166.inzchat.file.wrapper.ConnectionFileWrapper;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.*;
import java.util.List;
import java.util.Optional;

@Repository
public class FileNativeRepositoryImpl implements FileNativeRepository {
    static Logger logger = LoggerFactory.getLogger(FileNativeRepositoryImpl.class);

    @Autowired
    private Connection connection;


    @Override
    public Optional<ConnectionFileWrapper> getFile(Long id, String ownerName) {
        try {
            connection.setAutoCommit(false);

            LargeObjectManager lobj = connection.unwrap(org.postgresql.PGConnection.class).getLargeObjectAPI();

            PreparedStatement preparedStatement = connection.prepareStatement(
                    "select file, file_name, file_size from file f left join owner o on o.file_id = f.id " +
                            "where (f.id = ? and o.owner_name is null) or (f.id = ? and o.owner_name is not null and o.owner_name = ?)"
            );
            preparedStatement.setInt(1, id.intValue());
            preparedStatement.setInt(2, id.intValue());
            preparedStatement.setString(3, ownerName);

            ResultSet rs = preparedStatement.executeQuery();
            boolean res = rs.next();

            if (res) {
                Long blob = rs.getLong(1);
                String fileName = rs.getString(2);
                int fileSize = rs.getInt(3);

                LargeObject largeObject = lobj.open(blob);

                ConnectionFileWrapper connectionFileWrapper = ConnectionFileWrapper.builder()
                        .fileName(fileName)
                        .fileSize(fileSize)
                        .inputStream(largeObject.getInputStream())
                        .preparedStatement(preparedStatement)
                        .build();

                return Optional.of(connectionFileWrapper);
            }

        } catch (SQLException ex) {
            logger.error(ex.getMessage(), ex);
        }

        return Optional.empty();
    }

    @Override
    public FileDTO createFile(String filename, InputStream inputStream, List<String> owners) {
        try {
            connection.setAutoCommit(false);

            LargeObjectManager lobj = connection.unwrap(PgConnection.class).getLargeObjectAPI();

            long oid = lobj.createLO(LargeObjectManager.READ | LargeObjectManager.WRITE);
            LargeObject obj = lobj.open(oid, LargeObjectManager.WRITE);

            byte[] buffer = new byte[0x4ff];
            while (inputStream.read(buffer) != -1)
                obj.write(buffer);

            PreparedStatement statement = connection.prepareStatement("insert into file(file_name, file, file_size) values (?, ?, ?)", Statement.RETURN_GENERATED_KEYS);

            statement.setString(1, filename);
            statement.setLong(2, oid);
            statement.setInt(3, obj.size());

            obj.close();

            statement.executeUpdate();
            ResultSet keys = statement.getGeneratedKeys();
            long key = -1L;
            if (keys.next()) {
                key = keys.getLong(1);
            }
            statement.close();

            insertOwnersToFile(key, owners);
            return FileDTO.builder().filename(filename).id(key).build();
        } catch (SQLException | IOException ex) {
            logger.error(ex.getMessage(), ex);
        }
        return null;
    }

    private void insertOwnersToFile(Long id, List<String> owners) {
        try {
            PreparedStatement statement = connection.prepareStatement("insert into owner(owner_name, file_id) values (?, ?)");

            owners.forEach(e -> {
                try {
                    statement.setString(1, e);
                    statement.setInt(2, id.intValue());
                    statement.executeUpdate();
                } catch (SQLException ex) {
                    logger.error(ex.getMessage(), ex);
                }
            });

            statement.close();
        } catch (SQLException ex) {
            logger.error(ex.getMessage(), ex);
        }
    }
}
