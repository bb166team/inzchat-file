package pl.bb166.inzchat.file.service;

import pl.bb166.inzchat.file.domain.dto.FileDTO;
import pl.bb166.inzchat.file.wrapper.ConnectionFileWrapper;

import java.io.InputStream;
import java.util.List;
import java.util.Optional;

public interface FileService {
    FileDTO addFileToRepository(String fileName, InputStream input, List<String> owners);
    Optional<ConnectionFileWrapper> getFileForId(Long id, String ownerName);
}