package pl.bb166.inzchat.file.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.bb166.inzchat.file.domain.dto.FileDTO;
import pl.bb166.inzchat.file.repository.FileNativeRepository;
import pl.bb166.inzchat.file.wrapper.ConnectionFileWrapper;

import java.io.InputStream;
import java.util.List;
import java.util.Optional;

@Service
public class FileServiceImpl implements FileService {

    private FileNativeRepository fileNativeRepository;

    @Autowired
    public void setFileNativeRepository(FileNativeRepository fileNativeRepository) {
        this.fileNativeRepository = fileNativeRepository;
    }

    @Override
    public FileDTO addFileToRepository(String filename, InputStream inputStream, List<String> owners) {
        return fileNativeRepository.createFile(filename, inputStream, owners);
    }

    @Override
    public Optional<ConnectionFileWrapper> getFileForId(Long id, String ownerName) {
        return fileNativeRepository.getFile(id, ownerName);
    }
}
