package pl.bb166.inzchat.file.wrapper;

import lombok.Builder;
import lombok.Getter;

import java.io.Closeable;
import java.io.InputStream;
import java.sql.PreparedStatement;

@Builder
public class ConnectionFileWrapper implements Closeable {
    @Getter
    private PreparedStatement preparedStatement;
    @Getter
    private String fileName;
    @Getter
    private InputStream inputStream;
    @Getter
    private int fileSize;

    @Override
    public void close() {
        try {
            inputStream.close();
            preparedStatement.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
